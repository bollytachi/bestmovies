package bolly.bestmovies;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bolly.bestmovies.mFragment.MyPagerAdapter;
import bolly.bestmovies.mFragment.Ptvshow_fragment;

public class MainActivity extends AppCompatActivity {

    private ViewPager vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        vp = findViewById(R.id.viewpager_activity_main);

        this.addPages();

    }

    private void addPages() {

        MyPagerAdapter pagerAdapter = new MyPagerAdapter(this.getSupportFragmentManager());

        pagerAdapter.addFragment(new Ptvshow_fragment());

        //Set adapter to viewpager
        vp.setAdapter(pagerAdapter);

    }

}
