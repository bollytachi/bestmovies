package bolly.bestmovies.mFragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bolly.bestmovies.MySingleton;
import bolly.bestmovies.R;
import bolly.bestmovies.listviewAdapter.PtvshowCustomAdapter;
import bolly.bestmovies.listviewAdapter.TvShowDetailsCustomAdapter;

import static com.google.android.gms.internal.zzagz.runOnUiThread;


public class Ptvshow_fragment extends Fragment {

    private static ArrayList<JSONObject> tvShows = new ArrayList<>();
    private static ArrayList<JSONObject> tvShow_similar = new ArrayList<>();
    private ListView lv;
    private PtvshowCustomAdapter adapter;
    private View rootView;
    private ProgressBar progressBar;
    private Dialog dialog;
    private ProgressBar progressBar_similar;
    private ListView lv_similar;
    private TvShowDetailsCustomAdapter adapter_similar;
    private int j = 1;
    private String url = "https://api.themoviedb.org/3/tv/popular?api_key=1d31993a4b93ef01fc5bef73b6f8d17c&language=en-US&page=" + j;

    private String TAG = "PTVShowFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i(TAG, "OnCreateView.......");

        rootView = inflater.inflate(R.layout.ptvshow_fragment, container, false);

        progressBar = rootView.findViewById(R.id.progressBar_ptvshow_fragment);

        lv = rootView.findViewById(R.id.listview_ptvshow_fragment);

        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {

                    progressBar.setVisibility(View.VISIBLE);

                    j++;
                    String url = "https://api.themoviedb.org/3/tv/popular?api_key=1d31993a4b93ef01fc5bef73b6f8d17c&language=en-US&page=" + j;

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (response.length() != 0) {

                                        try {

                                            JSONArray jsonArray = response.getJSONArray("results");

                                            Log.i(TAG, "JsonResponse >>>>> " + jsonArray.get(1).toString());

                                            for (int i = 0; i < jsonArray.length(); i++) {

                                                tvShows.add(jsonArray.getJSONObject(i));

                                            }

                                            adapter.notifyDataSetChanged();

                                            progressBar.setVisibility(View.INVISIBLE);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            });

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i(TAG, "Error >>>>>>> " + error.getMessage());
                        }
                    });

                    MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                        if (adapter_similar != null) {
                            adapter_similar.notifyDataSetChanged();
                            adapter_similar = null;
                            if (lv_similar != null) {
                                lv_similar.setAdapter(null);
                            }
                        }

                        if (!tvShow_similar.isEmpty()) {
                            tvShow_similar.clear();
                        }

                        showPopupTvShowDetails(tvShows.get(position));

                    }
                });
            }
        });

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.setVisibility(View.VISIBLE);

                        if (response.length() != 0) {

                            Log.i(TAG, "JsonResponse >>>>> " + response.toString());

                            try {

                                JSONArray jsonArray = response.getJSONArray("results");

                                Log.i(TAG, "JsonResponse >>>>> " + jsonArray.get(1).toString());

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    tvShows.add(jsonArray.getJSONObject(i));

                                }

                                adapter = new PtvshowCustomAdapter(rootView.getContext(), tvShows);

                                lv.setAdapter(adapter);

                                progressBar.setVisibility(View.INVISIBLE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {

                            Log.i(TAG, "JsonResponse >>>>> is empty");

                        }

                    }
                });

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "Error Json: " + error.toString());

            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        return rootView;

    }

    private void showPopupTvShowDetails(final JSONObject jsonObject) {

        int j = 1;

        String url_similar = null;

        try {
            url_similar = "https://api.themoviedb.org/3/tv/" + jsonObject.getString("id") + "/similar?api_key=1d31993a4b93ef01fc5bef73b6f8d17c&language=en-US&page=" + j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new Dialog(getContext());

        dialog.setContentView(R.layout.tvshow_details);

        progressBar_similar = dialog.findViewById(R.id.progressBar_tvshow_details_fragment);

        lv_similar = dialog.findViewById(R.id.listview_tvshow_details_fragment);

        String url_details = null;
        try {
            url_details = "https://image.tmdb.org/t/p/original" + jsonObject.getString("poster_path");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ImageView imageView_tvshow_model = dialog.findViewById(R.id.imageView_tvshow_details);

        Picasso.with(getContext()).load(url_details).fit().into(imageView_tvshow_model);

        TextView name_tvshow_details, date_tvshow_details, overview_tvshow_details;

        name_tvshow_details = dialog.findViewById(R.id.name_tvshow_details);
        date_tvshow_details = dialog.findViewById(R.id.date_tvshow_details);
        overview_tvshow_details = dialog.findViewById(R.id.overview_tvshow_details);

        try {

            name_tvshow_details.setText(jsonObject.getString("name"));
            date_tvshow_details.setText(jsonObject.getString("first_air_date"));
            overview_tvshow_details.setText(jsonObject.getString("overview"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        progressBar_similar.setVisibility(View.VISIBLE);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url_similar, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar_similar.setVisibility(View.VISIBLE);

                        if (response.length() != 0) {

                            try {

                                JSONArray jsonArray = response.getJSONArray("results");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    tvShow_similar.add(jsonArray.getJSONObject(i));

                                }

                                adapter_similar = new TvShowDetailsCustomAdapter(dialog.getContext(), tvShow_similar);

                                lv_similar.setAdapter(adapter_similar);

                                progressBar_similar.setVisibility(View.INVISIBLE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {

                            Log.i(TAG, "JsonResponse >>>>> is empty");

                        }
                    }
                });

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "Error Json 1 function: " + error.toString());

            }
        });

        MySingleton.getInstance(dialog.getContext()).addToRequestQueue(jsonObjectRequest);

    }


}
