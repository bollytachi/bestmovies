package bolly.bestmovies.listviewAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bolly.bestmovies.R;


public class TvShowDetailsCustomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<JSONObject> tvShows;
    private LayoutInflater inflater;
    private TextView date, name;
    private String url;
    private ImageView imageView_tvshow_model;

    private String TAG = "TVShowDetailsCustomAdapter";

    public TvShowDetailsCustomAdapter(Context context, ArrayList<JSONObject> tvShows) {
        this.context = context;
        this.tvShows = tvShows;
    }

    @Override
    public int getCount() {
        return tvShows.size();
    }

    @Override
    public Object getItem(int position) {
        return tvShows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.i(TAG, "TVShowDetailsCustomAdapter......................");

        if (inflater == null) {

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.tvshow_detail_model, parent, false);

        }

        if (!tvShows.isEmpty()) {

            date = convertView.findViewById(R.id.date_tvshow_details_model);
            name = convertView.findViewById(R.id.name_tvshow_details_model);

            try {

                date.setText(tvShows.get(position).getString("first_air_date"));
                name.setText(tvShows.get(position).getString("name"));

                url = "https://image.tmdb.org/t/p/w500" + tvShows.get(position).getString("poster_path");

                imageView_tvshow_model = convertView.findViewById(R.id.imageView_tvshow_details_model);

                Picasso.with(context).load(url).fit().into(imageView_tvshow_model);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            notifyDataSetChanged();

        } else {

            Log.i(TAG, "tvshows data.....is empty");
            notifyDataSetChanged();

        }

        return convertView;
    }

}
