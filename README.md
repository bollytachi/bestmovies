# Autor : Bolly DOUNLA TACHI

Display popular TV shows from themoviedb API in an Android mobile application

The main requirements are: 

    
    *  Fetch a list of popular TV shows using that endpoint: https://developers.themoviedb.org/3/tv/get-popular-tv-shows. When users scroll to the bottom fetch the next page of shows. 
   
    *   When a user clicks on the show it should display show’s details. Use a tv show detail API endpoint that you can find exploring themovedb API documentation. It’s up to you which information you display. 
    
    *  Bonus task: in the detail view, the app should display similar tv shows (https://developers.themoviedb.org/3/tv/get-similar-tv-shows). One page is enough, don’t implement paging here. 

